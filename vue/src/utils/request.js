/**
 * 使用axios库进行网络请求
 */

import axios from 'axios'
import router from "@/router";
import Cookies from 'js-cookie'

const request = axios.create({
    baseURL: 'http://localhost:9090/api',   // 统一后台端口前缀（配合JWT安全检验而添加"/api"）
    timeout: 5000
})

// 注册 request 拦截器
// 可以自请求发送前对请求做一些处理
// 比如统一加token，对请求参数统一加密
request.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/json;charset=utf-8';

    // 从Cookies中取出登录信息
    const adminJson = Cookies.get('admin')
    if (adminJson) {
        // 设置解析后的token到请求头
        config.headers['token'] = JSON.parse(adminJson).token
    }
    return config
}, error => {
    return Promise.reject(error)
});

// 注册 response 拦截器
// 可以在接口响应后统一处理结果
request.interceptors.response.use(
    response => {
        // 获取响应的数据
        let res = response.data;
        // 兼容服务端返回的字符串数据
        if (typeof res === 'string') {
            res = res ? JSON.parse(res) : res
        }
        // JWT返回401，未经登录不允许访问
        if (res.code === '401') {
            router.push('/login')
        }
        return res;
    },
    error => {
        console.log('err' + error) // for debug
        return Promise.reject(error)
    }
)

// 将 request 实例默认导出，使其在其他模块中可使用
export default request