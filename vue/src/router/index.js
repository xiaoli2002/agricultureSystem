import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../views/Layout.vue'
import Cookies from "js-cookie";

Vue.use(VueRouter)

const routes = [
  //  ====== 登录  =====
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/Login.vue'), //导入组件
  },
    //  ====== 主页  =====
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: '/home',  // 父路由的重定向，仅当path为".../"，重定向到".../home"，而不影响".../userList"等子路由（参看Layout.js）
    children: [
      {
        path: 'home',
        name: 'Home',
        component: () => import('@/views/home/HomeView.vue'),
      },
      //  ====  Car  ====
      {
        path: 'carList',
        name: 'carList',
        component: () => import('@/views/car/List.vue'),
      },
      {
        path: 'addCar',
        name: 'AddCar',
        component: () => import('@/views/car/Add.vue'),
      },
      {
        path: 'editCar',
        name: 'EditCar',
        component: () => import('@/views/car/Edit.vue'),
      },
      //  ====  Admin  ====
      {
        path: 'adminList',
        name: 'AdminList',
        component: () => import('@/views/admin/List.vue'),
      },
      {
        path: 'addAdmin',
        name: 'AddAdmin',
        component: () => import('@/views/admin/Add.vue'),
      },
      {
        path: 'editAdmin',
        name: 'EditAdmin',
        component: () => import('@/views/admin/Edit.vue'),
      },
      //  ====  DiseaseCategory & Disease  ====
      { path: 'diseaseCategoryList', name: 'DiseaseCategoryList', component: () => import('@/views/diseaseCategory/List.vue') },
      { path: 'addDiseaseCategory', name: 'AddDiseaseCategory', component: () => import('@/views/diseaseCategory/Add.vue') },
      { path: 'editDiseaseCategory', name: 'EditDiseaseCategory', component: () => import('@/views/diseaseCategory/Edit.vue') },
      { path: 'diseaseList', name: 'DiseaseList', component: () => import('@/views/disease/List.vue') },
      { path: 'addDisease', name: 'AddDisease', component: () => import('@/views/disease/Add.vue') },
      { path: 'editDisease', name: 'EditDisease', component: () => import('@/views/disease/Edit.vue') },
      //  ====  BugCategory & Bug ====
      { path: 'bugCategoryList', name: 'BugCategoryList', component: () => import('@/views/bugCategory/List.vue') },
      { path: 'addBugCategory', name: 'AddBugCategory', component: () => import('@/views/bugCategory/Add.vue') },
      { path: 'editBugCategory', name: 'EditBugCategory', component: () => import('@/views/bugCategory/Edit.vue') },
      { path: 'bugList', name: 'BugList', component: () => import('@/views/bug/List.vue') },
      { path: 'addBug', name: 'AddBug', component: () => import('@/views/bug/Add.vue') },
      { path: 'editBug', name: 'EditBug', component: () => import('@/views/bug/Edit.vue') },
      //  ====  Garden  ====
      { path: 'gardenList', name: 'GardenList', component: () => import('@/views/garden/List.vue') },
      { path: 'addGarden', name: 'AddGarden', component: () => import('@/views/garden/Add.vue') },
      { path: 'editGarden', name: 'EditGarden', component: () => import('@/views/garden/Edit.vue') },
      //  ====  Completion  ====
      { path: 'completionList', name: 'completionList', component: () => import('@/views/completion/List.vue') },
    ]
  },
  {
    path: "*",
    component:() => import('@/views/404')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// =========全局前置路由守卫===========
// 在每次路由切换前执行，调用 next() 以重定向到目标路由
router.beforeEach((to, from, next) => {
  if (to.path === '/login') next()
  const admin = Cookies.get("admin")
  if (!admin && to.path !== '/login') return next("/login")  // 强制退回到登录页面
  // 访问 /home 的时候，并且cookie里面存在数据，这个时候我就直接放行
  next()
})

export default router
