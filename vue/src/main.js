import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 导入elementUI组件
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// 启用样式
import '@/assets/global.css'
// 导入滑块验证组件
import SlideVerify from 'vue-monoplasty-slide-verify';
// 启用elementUI
Vue.use(SlideVerify);
Vue.config.productionTip = false
Vue.use(ElementUI, { size: 'small' });  // medium  small mini

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
