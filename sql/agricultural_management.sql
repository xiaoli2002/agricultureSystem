/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : agricultural_management

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 03/09/2023 21:16:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户名',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '联系方式',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `createtime` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updatetime` datetime NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username_key`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', '13988997788', NULL, '2022-10-08 22:21:26', '2022-10-11 20:37:20', 'ead08e95e9c867a2ee833c232bf55dbc', 1);
INSERT INTO `admin` VALUES (2, 'admin1', '13877889900', 'admin1@qq.com', '2022-10-10 21:28:42', '2023-08-29 19:51:02', '04f604bf95d330e9488ef5842bd5080e', 1);
INSERT INTO `admin` VALUES (3, '小红兰', '19163333444', '2356892313@qq.com', '2023-08-31 21:34:47', NULL, '77e3b13fb300c6f66e1e2568313c812a', 1);
INSERT INTO `admin` VALUES (4, '周杰伦', '16466111111', '1234545611@qq.com', '2023-08-31 21:35:17', NULL, '77e3b13fb300c6f66e1e2568313c812a', 1);
INSERT INTO `admin` VALUES (5, '黄飞飞', '16666666666', '1234545611@qq.com', '2023-08-31 21:35:29', NULL, '77e3b13fb300c6f66e1e2568313c812a', 1);

-- ----------------------------
-- Table structure for bug
-- ----------------------------
DROP TABLE IF EXISTS `bug`;
CREATE TABLE `bug`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `appear_date` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '出现日期',
  `treatment_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '防治方法',
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `bug_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '分类',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '图片',
  `createtime` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updatetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `server_level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '严重程度',
  `bugNo` int NULL DEFAULT NULL COMMENT '病害编号',
  `appear_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '出现地点',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bug
-- ----------------------------
INSERT INTO `bug` VALUES (57, '蚜虫附叶病', '蚜虫繁多，体型小巧，有时会聚集成群，有些蚜虫具有翅膀，可以进行短距离的飞行，从一株植物传播到另一株植物上。', '2023-08-15', '药物治疗', NULL, '蚜科病 > 蚜虫', 'http://localhost:9090/api/bug/file/download/1693491444585?&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxIiwiZXhwIjoxNjk0Nzg3NDQ0fQ.acSorvx7mpaFDOU48pTFBgeSqWEPajVnlWdOpR_8lQE&play=1', '2023-08-31 22:17:25', NULL, '轻度', 0, '农田区域5');
INSERT INTO `bug` VALUES (58, '苹果虫', '苹果被虫子吃，造成园区30%左右的损失', '2023-08-16', '药物防治', NULL, '蚜科病 > 苹果虫', 'http://localhost:9090/api/bug/file/download/1693491522260?&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxIiwiZXhwIjoxNjk0Nzg3NTIyfQ.OFl8uxInlA9gFEErrrVQQye5amo1jem9DhrZKKRJtko&play=1', '2023-08-31 22:18:43', NULL, '中度', 0, '农园区域1');
INSERT INTO `bug` VALUES (59, '烟粉虱', '聚集在植物的叶片、茎部和花朵上，寄生并通过吸食植物的汁液来获取营养。', '2023-08-28', NULL, NULL, '蚜科病 > 烟虱', 'http://localhost:9090/api/bug/file/download/1693491582921?&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxIiwiZXhwIjoxNjk0Nzg3NTgyfQ.NI0AIHpXiqB6A1SKi28QunjjWcx2Bt8q1fdgYyDjaTw&play=1', '2023-08-31 22:19:44', NULL, '重度', 0, '农园区域2');

-- ----------------------------
-- Table structure for bugcategory
-- ----------------------------
DROP TABLE IF EXISTS `bugcategory`;
CREATE TABLE `bugcategory`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '编号',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `pid` int NULL DEFAULT NULL COMMENT '父级id',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updatetime` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `pno` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '一级编号',
  `sno` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '二级编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bugcategory
-- ----------------------------
INSERT INTO `bugcategory` VALUES (46, '蚜科病', 'OPD', '蚜虫导致的植物病', NULL, '2023-08-31 22:12:03', NULL, NULL, 'OPD');
INSERT INTO `bugcategory` VALUES (47, '蚜虫', 'OPD-001', '蚜虫', 46, '2023-08-31 22:14:39', NULL, 'OPD', '001');
INSERT INTO `bugcategory` VALUES (48, '苹果虫', 'OPD-002', NULL, 46, '2023-08-31 22:14:54', NULL, 'OPD', '002');
INSERT INTO `bugcategory` VALUES (49, '烟虱', 'OPD-003', NULL, 46, '2023-08-31 22:15:11', NULL, 'OPD', '003');
INSERT INTO `bugcategory` VALUES (50, '螨虫科', 'OOA', NULL, NULL, '2023-08-31 22:15:47', NULL, NULL, 'OOA');
INSERT INTO `bugcategory` VALUES (51, '食叶害虫科', 'BGH', NULL, NULL, '2023-08-31 22:16:07', NULL, NULL, 'BGH');

-- ----------------------------
-- Table structure for car
-- ----------------------------
DROP TABLE IF EXISTS `car`;
CREATE TABLE `car`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '编号',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '工作地点',
  `createtime` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `updatetime` datetime NULL DEFAULT NULL,
  `pesticide` int NULL DEFAULT 0 COMMENT '农药余量（%）',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '禁用状态 1不禁用',
  `admin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '管理员',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username_index`(`no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of car
-- ----------------------------
INSERT INTO `car` VALUES (48, '农车一号', 'K210', '田园区域1', '2023-08-31 21:45:41', NULL, 32, 1, 'admin');
INSERT INTO `car` VALUES (49, '农车二号', 'K335', '田园区域2', '2023-08-31 21:45:52', NULL, 10, 1, 'admin');
INSERT INTO `car` VALUES (50, '农车三号', 'D220', '田园区域3', '2023-08-31 21:46:03', NULL, 50, 1, 'admin');
INSERT INTO `car` VALUES (51, '农车四号', 'K257', '田园区域4', '2023-08-31 21:46:33', '2023-08-31 21:46:50', 98, 0, 'admin');
INSERT INTO `car` VALUES (52, '农车五号', 'L102', '田园区域5', '2023-08-31 21:46:46', '2023-09-02 16:21:32', 0, 0, 'admin');

-- ----------------------------
-- Table structure for completion
-- ----------------------------
DROP TABLE IF EXISTS `completion`;
CREATE TABLE `completion`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `garden_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '区域名称',
  `garden_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '区域编号',
  `garden_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '区域地址',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '负责人名称',
  `user_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '负责人联系方式',
  `start_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '开始时间',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '已借出' COMMENT '种植状态',
  `days` int NULL DEFAULT 1 COMMENT '预期种植天数',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `crop` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '种植作物',
  `real_time` datetime NULL DEFAULT NULL COMMENT '确认收获时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of completion
-- ----------------------------
INSERT INTO `completion` VALUES (55, '农园区域2', '002', NULL, '虫大', '11222222112', '2023-08-23 00:00:00', '已收获', 3, '2023-08-26 00:00:00', '茄子', '2023-09-03 00:00:00');
INSERT INTO `completion` VALUES (56, '农园区域3', '003', NULL, '黎娃', '111111111111', '2023-08-29 00:00:00', '已收获', 4, '2023-09-02 00:00:00', '苹果', '2023-09-03 00:00:00');

-- ----------------------------
-- Table structure for disease
-- ----------------------------
DROP TABLE IF EXISTS `disease`;
CREATE TABLE `disease`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `appear_date` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '出现日期',
  `treatment_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '防治方法',
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `disease_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '分类',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '图片',
  `createtime` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updatetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `server_level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '严重程度',
  `diseaseNo` int NULL DEFAULT NULL COMMENT '病害编号',
  `appear_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '出现地点',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of disease
-- ----------------------------
INSERT INTO `disease` VALUES (54, '葡萄霜霉病', '叶片上出现白色或灰白色的霉菌孢子形成的粉状斑点', '2023-08-16', '药引治疗', '需要早治疗', '葡萄科病 > 葡萄霜霉病', 'http://localhost:9090/api/disease/file/download/1693490465872?&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxIiwiZXhwIjoxNjk0Nzg2NDY1fQ.gR3NfH0i1kGbjrpNZ7Vo6HxlrELbzseLevtA-KYlNtQ&play=1', '2023-08-31 22:01:20', NULL, '轻度', 0, '田园区域1');
INSERT INTO `disease` VALUES (55, '番茄蕨叶病', '受感染的番茄植株的叶片会发生异常的变形，通常变得扭曲或呈现类似蕨叶的形状。', '2023-08-23', NULL, NULL, '草本植物病 > 番茄蕨叶病', 'http://localhost:9090/api/disease/file/download/1693490553757?&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxIiwiZXhwIjoxNjk0Nzg2NTUzfQ.p_N0IT4dh3Bf1AjoudrYkF4juf1P2MrVtmESwF_sPCE&play=1', '2023-08-31 22:02:34', NULL, '中度', 0, '田园区域2');
INSERT INTO `disease` VALUES (56, '茄子青枯病', '受感染的茄子植株的叶片会逐渐出现黄化、萎缩和脱落。', '2023-08-28', '药引治疗', NULL, '草本植物病 > 茄子青枯病', 'http://localhost:9090/api/disease/file/download/1693490639177?&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxIiwiZXhwIjoxNjk0Nzg2NjM5fQ.hfsTTFoQmCDTm3wFKP_8rgO8y4A1ZMg3ojBEErMjcGQ&play=1', '2023-08-31 22:04:00', NULL, '轻度', 0, '田园区域3');

-- ----------------------------
-- Table structure for diseasecategory
-- ----------------------------
DROP TABLE IF EXISTS `diseasecategory`;
CREATE TABLE `diseasecategory`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '编号',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `pid` int NULL DEFAULT NULL COMMENT '父级id',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updatetime` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `pno` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '一级编号',
  `sno` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '二级编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of diseasecategory
-- ----------------------------
INSERT INTO `diseasecategory` VALUES (44, '草本植物病', 'CCK', '草本植物容易罹患', NULL, '2023-08-31 21:54:23', NULL, NULL, 'CCK');
INSERT INTO `diseasecategory` VALUES (45, '无叶病', 'KJA', '无叶病症', NULL, '2023-08-31 21:55:01', NULL, NULL, 'KJA');
INSERT INTO `diseasecategory` VALUES (46, '葡萄科病', 'POJ', NULL, NULL, '2023-08-31 21:55:50', NULL, NULL, 'POJ');
INSERT INTO `diseasecategory` VALUES (48, '葡萄霜霉病', 'POJ-001', '病', 46, '2023-08-31 21:57:31', NULL, 'POJ', '001');
INSERT INTO `diseasecategory` VALUES (49, '番茄蕨叶病', 'CCK-002', '番茄', 44, '2023-08-31 21:57:53', '2023-08-31 00:00:00', 'CCK', '002');
INSERT INTO `diseasecategory` VALUES (50, '茄子青枯病', 'CCK-003', NULL, 44, '2023-08-31 21:58:55', NULL, 'CCK', '003');

-- ----------------------------
-- Table structure for garden
-- ----------------------------
DROP TABLE IF EXISTS `garden`;
CREATE TABLE `garden`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `garden_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '区域名称',
  `garden_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '区域编号',
  `garden_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '区域地址',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '负责人名称',
  `user_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '负责人联系方式',
  `start_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '开始时间',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '种植中' COMMENT '种植状态',
  `days` int NULL DEFAULT 1 COMMENT '预期种植天数',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `crop` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '种植作物',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of garden
-- ----------------------------
INSERT INTO `garden` VALUES (47, '农园区域5', '005', NULL, '略略略', '15552225552', '2023-08-23 00:00:00', '种植中', 24, '2023-09-16 00:00:00', '摇篮百合', NULL);
INSERT INTO `garden` VALUES (48, '农园区域4', '004', NULL, '黄玲玲', '1251333333', '2023-08-01 00:00:00', '种植中', 100, '2023-11-09 00:00:00', '葡萄', NULL);
INSERT INTO `garden` VALUES (52, '农园区域1', '001', NULL, '黄奕为', '13666666666', '2023-08-14 00:00:00', '种植中', 24, '2023-09-07 00:00:00', '玉米', NULL);

-- ----------------------------
-- Table structure for weather
-- ----------------------------
DROP TABLE IF EXISTS `weather`;
CREATE TABLE `weather`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `temperature` double NULL DEFAULT NULL COMMENT '单位：℃',
  `humidity` double NULL DEFAULT NULL COMMENT '单位：%',
  `rain_rate` double NULL DEFAULT NULL COMMENT '单位：%',
  `date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of weather
-- ----------------------------
INSERT INTO `weather` VALUES (1, 28, 12, 15, '2023-09-02 00:00:00');
INSERT INTO `weather` VALUES (2, 30, 10, 11, '2023-09-01 00:00:00');
INSERT INTO `weather` VALUES (3, 26, 15, 3, '2023-08-31 00:00:00');
INSERT INTO `weather` VALUES (4, 33, 22, 8, '2023-09-03 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
