package com.example.springboot.mapper;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.DiseaseCategory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DiseaseCategoryMapper {

    List<DiseaseCategory> list();

    List<DiseaseCategory> listByCondition(BaseRequest baseRequest);

    void save(DiseaseCategory obj);

    DiseaseCategory getById(Integer id);

    void updateById(DiseaseCategory user);

    void deleteById(Integer id);

}
