package com.example.springboot.mapper;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.BugCategory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BugCategoryMapper {

    List<BugCategory> list();

    List<BugCategory> listByCondition(BaseRequest baseRequest);

    void save(BugCategory obj);

    BugCategory getById(Integer id);

    void updateById(BugCategory user);

    void deleteById(Integer id);

}
