package com.example.springboot.mapper;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Weather;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface WeatherMapper {

    List<Weather> list();

    List<Weather> listByCondition(BaseRequest baseRequest);

    Weather getByDate(LocalDate date);

    List<Weather> getDataByTimeRange(@Param("timeRange") String timeRange);

}
