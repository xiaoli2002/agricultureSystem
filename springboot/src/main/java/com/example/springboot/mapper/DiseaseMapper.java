package com.example.springboot.mapper;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Disease;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DiseaseMapper {

    List<Disease> list();

    List<Disease> listByCondition(BaseRequest baseRequest);

    void save(Disease obj);

    Disease getById(Integer id);

    void updateById(Disease user);

    void deleteById(Integer id);

    Disease getByNo(String diseaseNo);

}
