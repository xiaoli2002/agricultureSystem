package com.example.springboot.mapper;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Bug;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BugMapper {

    List<Bug> list();

    List<Bug> listByCondition(BaseRequest baseRequest);

    void save(Bug obj);

    Bug getById(Integer id);

    void updateById(Bug user);

    void deleteById(Integer id);

    Bug getByNo(String bugNo);

}
