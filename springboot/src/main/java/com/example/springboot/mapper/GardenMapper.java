package com.example.springboot.mapper;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Garden;
import com.example.springboot.entity.Completion;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface GardenMapper {

    List<Garden> list();

    List<Garden> listByCondition(BaseRequest baseRequest);

    List<Completion> listCompletionByCondition(BaseRequest baseRequest);

    void save(Garden obj);

    void saveCompletion(Completion obj);

    void backCompletion(Completion obj);

    Garden getById(Integer id);

    Garden getCompletionById(Integer id);

    void updateById(Garden garden);

    void deleteById(Integer id);

    void deleteCompletionById(Integer id);

    void updateStatus(String status, Integer id);

}
