package com.example.springboot.mapper;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Car;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CarMapper {

    List<Car> list();

    List<Car> listByCondition(BaseRequest baseRequest);

    void save(Car car);

    Car getById(Integer id);

    void updateById(Car car);

    void deleteById(Integer id);

    Car getByCarname(String carname);

}
