package com.example.springboot.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class Garden {

    private Integer id;
    private String gardenName;
    private String gardenNo;
    private String gardenAddress;
    private String crop;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate startTime;
    private String status;
    private Integer days;
    private LocalDate endTime;
    private String userName;
    private String userPhone;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate updateTime;
    // 提醒状态
    private String note;

}
