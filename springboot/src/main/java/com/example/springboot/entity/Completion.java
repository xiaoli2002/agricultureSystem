package com.example.springboot.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class Completion {

    private Integer id;
    private String gardenName;
    private String gardenNo;
    private String userName;
    private String userPhone;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate startTime;
    private String crop;
    private String status;
    private Integer days;
    private LocalDate endTime;
    private LocalDate realTime;

}
