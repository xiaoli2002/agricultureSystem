package com.example.springboot.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Disease extends BaseEntity implements Serializable {

    private String name;

    private String diseaseCategory;

    private String appearDate;

    private String treatmentMethod;

    private String appearLocation;

    private String serverLevel;

    private String picture;

    private String description;

    private String note;

    private String diseaseNo;

    private List<String> diseaseCategories;

}
