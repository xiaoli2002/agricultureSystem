package com.example.springboot.controller.request;

import lombok.Data;

/* 通用信息类 */
@Data
public class BaseRequest {
    private Integer pageNum = 1;    //页码
    private Integer pageSize = 10;  //显示的页数
}
