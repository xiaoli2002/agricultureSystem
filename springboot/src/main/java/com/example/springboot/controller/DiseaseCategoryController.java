package com.example.springboot.controller;

import cn.hutool.core.collection.CollUtil;
import com.example.springboot.common.Result;
import com.example.springboot.controller.request.DiseaseCategoryPageRequest;
import com.example.springboot.entity.DiseaseCategory;
import com.example.springboot.service.IDiseaseCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/diseaseCategory")
public class DiseaseCategoryController {

    @Autowired
    IDiseaseCategoryService diseaseCategoryService;

    @PostMapping("/save")
    public Result save(@RequestBody DiseaseCategory obj) {
        diseaseCategoryService.save(obj);
        return Result.success();
    }

    @PutMapping("/update")
    public Result update(@RequestBody DiseaseCategory obj) {
        diseaseCategoryService.update(obj);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        diseaseCategoryService.deleteById(id);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result getById(@PathVariable Integer id) {
        DiseaseCategory obj = diseaseCategoryService.getById(id);
        return Result.success(obj);
    }

    @GetMapping("/list")
    public Result list() {
        List<DiseaseCategory> list = diseaseCategoryService.list();
        return Result.success(list);
    }

    // 分类分级
    @GetMapping("/tree")
    public Result tree() {
        List<DiseaseCategory> list = diseaseCategoryService.list();
        return Result.success(createTree(null, list));   //  null 表示从第一级开始递归
    }

    // 完全递归的方法来实现递归树
    private List<DiseaseCategory> createTree(Integer pid, List<DiseaseCategory> diseaseCategories) {
        List<DiseaseCategory> treeList = new ArrayList<>();
        for (DiseaseCategory diseaseCategory : diseaseCategories) {
            if (pid == null) {
                if (diseaseCategory.getPid() == null) {  // 那这就是第一级节点
                    treeList.add(diseaseCategory);
                    diseaseCategory.setChildren(createTree(diseaseCategory.getId(), diseaseCategories));
                }
            } else {
                if (pid.equals(diseaseCategory.getPid())) {
                    treeList.add(diseaseCategory);
                    diseaseCategory.setChildren(createTree(diseaseCategory.getId(), diseaseCategories));
                }
            }
            if (CollUtil.isEmpty(diseaseCategory.getChildren())) {
                diseaseCategory.setChildren(null);
            }
        }
        return treeList;
    }

    @GetMapping("/page")
    public Result page(DiseaseCategoryPageRequest pageRequest) {
        return Result.success(diseaseCategoryService.page(pageRequest));
    }

}
