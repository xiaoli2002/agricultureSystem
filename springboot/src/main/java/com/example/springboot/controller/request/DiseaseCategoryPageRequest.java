package com.example.springboot.controller.request;

import lombok.Data;

@Data
public class DiseaseCategoryPageRequest extends BaseRequest{
    private String name;
    private String no;
}
