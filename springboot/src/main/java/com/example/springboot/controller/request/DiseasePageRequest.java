package com.example.springboot.controller.request;

import lombok.Data;

@Data
public class DiseasePageRequest extends BaseRequest{
    private String name;
    private String diseaseNo;
}
