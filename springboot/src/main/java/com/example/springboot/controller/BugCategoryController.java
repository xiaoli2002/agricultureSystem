package com.example.springboot.controller;

import cn.hutool.core.collection.CollUtil;
import com.example.springboot.common.Result;
import com.example.springboot.controller.request.BugCategoryPageRequest;
import com.example.springboot.entity.BugCategory;
import com.example.springboot.service.IBugCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/bugCategory")
public class BugCategoryController {

    @Autowired
    IBugCategoryService bugCategoryService;

    @PostMapping("/save")
    public Result save(@RequestBody BugCategory obj) {
        bugCategoryService.save(obj);
        return Result.success();
    }

    @PutMapping("/update")
    public Result update(@RequestBody BugCategory obj) {
        bugCategoryService.update(obj);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        bugCategoryService.deleteById(id);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result getById(@PathVariable Integer id) {
        BugCategory obj = bugCategoryService.getById(id);
        return Result.success(obj);
    }

    @GetMapping("/list")
    public Result list() {
        List<BugCategory> list = bugCategoryService.list();
        return Result.success(list);
    }

    // 分类分级
    @GetMapping("/tree")
    public Result tree() {
        List<BugCategory> list = bugCategoryService.list();
        return Result.success(createTree(null, list));   //  null 表示从第一级开始递归
    }

    // 完全递归的方法来实现递归树
    private List<BugCategory> createTree(Integer pid, List<BugCategory> bugCategories) {
        List<BugCategory> treeList = new ArrayList<>();
        for (BugCategory bugCategory : bugCategories) {
            if (pid == null) {
                if (bugCategory.getPid() == null) {  // 那这就是第一级节点
                    treeList.add(bugCategory);
                    bugCategory.setChildren(createTree(bugCategory.getId(), bugCategories));
                }
            } else {
                if (pid.equals(bugCategory.getPid())) {
                    treeList.add(bugCategory);
                    bugCategory.setChildren(createTree(bugCategory.getId(), bugCategories));
                }
            }
            if (CollUtil.isEmpty(bugCategory.getChildren())) {
                bugCategory.setChildren(null);
            }
        }
        return treeList;
    }

    @GetMapping("/page")
    public Result page(BugCategoryPageRequest pageRequest) {
        return Result.success(bugCategoryService.page(pageRequest));
    }

}
