package com.example.springboot.controller;

import com.example.springboot.common.Result;
import com.example.springboot.controller.request.GardenPageRequest;
import com.example.springboot.entity.Garden;
import com.example.springboot.entity.Completion;
import com.example.springboot.service.IGardenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/garden")
public class GardenController {

    @Autowired
    IGardenService gardenService;

    @PostMapping("/save")
    public Result save(@RequestBody Garden obj) {
        gardenService.save(obj);
        return Result.success();
    }

    @PutMapping("/update")
    public Result update(@RequestBody Garden obj) {
        gardenService.update(obj);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        gardenService.deleteById(id);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result getById(@PathVariable Integer id) {
        Garden obj = gardenService.getById(id);
        return Result.success(obj);
    }

    @GetMapping("/list")
    public Result list() {
        List<Garden> list = gardenService.list();
        return Result.success(list);
    }

    @GetMapping("/page")
    public Result page(GardenPageRequest pageRequest) {
        return Result.success(gardenService.page(pageRequest));
    }

    @GetMapping("/pageCompletion")
    public Result pageCompletion(GardenPageRequest pageRequest) {
        return Result.success(gardenService.pageCompletion(pageRequest));
    }

    @PostMapping("/saveCompletion")
    public Result saveCompletion(@RequestBody Completion obj) {
        gardenService.saveCompletion(obj);
        return Result.success();
    }

    @PostMapping("/backCompletion")
    public Result backCompletion(@RequestBody Completion obj){
        gardenService.backCompletion(obj);
        return  Result.success();
    }

    @DeleteMapping("/deleteCompletion/{id}")
    public Result deleteCompletion(@PathVariable Integer id) {
        gardenService.deleteCompletionById(id);
        return Result.success();
    }

}
