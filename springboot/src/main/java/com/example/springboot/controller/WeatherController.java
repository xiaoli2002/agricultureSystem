package com.example.springboot.controller;

import com.example.springboot.common.Result;
import com.example.springboot.service.IWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/weather")
public class WeatherController {

    @Autowired
    IWeatherService weatherService;

    // timeRange: week month month2 month3
    @GetMapping("/lineCharts/{timeRange}")
    public Result lineCharts(@PathVariable String timeRange) {
        return Result.success(weatherService.getDataByTimeRange(timeRange));
    }

}
