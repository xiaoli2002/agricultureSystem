package com.example.springboot.controller.request;

import lombok.Data;

@Data
public class GardenPageRequest extends BaseRequest{
    private String gardenName;
    private String gardenNo;
    private String userName;
}
