package com.example.springboot.controller.request;

import lombok.Data;

@Data
public class BugPageRequest extends BaseRequest{
    private String name;
    private String bugNo;
}
