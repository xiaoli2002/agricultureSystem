package com.example.springboot.controller.request;

import lombok.Data;

/* 查询类 */
@Data
public class CarPageRequest extends BaseRequest{
    private String name;
    private String no;
}
