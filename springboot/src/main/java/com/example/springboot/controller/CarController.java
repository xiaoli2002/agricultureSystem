package com.example.springboot.controller;

import com.example.springboot.common.Result;
import com.example.springboot.controller.request.CarPageRequest;
import com.example.springboot.entity.Car;
import com.example.springboot.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/car")
public class CarController {

    @Autowired
    ICarService carService;

    @PostMapping("/save")
    public Result save(@RequestBody Car car) {
        carService.save(car);
        return Result.success();
    }


    @PostMapping("/pesticide")
    public Result pesticide(@RequestBody Car car) {
        carService.handlePesticide(car);
        return Result.success();
    }

    @PutMapping("/update")
    public Result update(@RequestBody Car car) {
        carService.update(car);
        return Result.success();
    }

    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id) {
        carService.deleteById(id);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result getById(@PathVariable Integer id) {
        Car car = carService.getById(id);
        return Result.success(car);
    }

    @GetMapping("/list")
    public Result list() {
        List<Car> cars = carService.list();    // 获取Json对象
        return Result.success(cars);           // 将数据进行存储
    }

    @GetMapping("/page")
    public Result page(CarPageRequest carPageRequest) {
        return Result.success(carService.page(carPageRequest));
    }

}
