package com.example.springboot.service;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Bug;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IBugService {
    
    List<Bug> list();

    PageInfo<Bug> page(BaseRequest baseRequest);

    void save(Bug obj);

    Bug getById(Integer id);

    void update(Bug obj);

    void deleteById(Integer id);

}
