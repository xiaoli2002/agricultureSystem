package com.example.springboot.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Weather;
import com.example.springboot.mapper.WeatherMapper;
import com.example.springboot.service.IWeatherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;

@Slf4j
@Service
public class WeatherService implements IWeatherService {

    @Resource
    WeatherMapper weatherMapper;

    @Override
    public List<Weather> list() {
        return null;
    }

    @Override
    public List<Weather> listByCondition(BaseRequest baseRequest) {
        return null;
    }

    @Override
    public Map<String, Object> getDataByTimeRange(String timeRange) {
        Map<String, Object> map = new HashMap<>();
        Date today = new Date();
        List<DateTime> dateRange;
        switch (timeRange) {
            case "week":
                // offsetDay 计算时间的一个工具方法
                // rangeToList 返回从开始时间到结束时间的一个时间范围
                dateRange = DateUtil.rangeToList(DateUtil.offsetDay(today, -6), today, DateField.DAY_OF_WEEK);
                break;
            case "month":
                dateRange = DateUtil.rangeToList(DateUtil.offsetDay(today, -29), today, DateField.DAY_OF_MONTH);
                break;
            case "month2":
                dateRange = DateUtil.rangeToList(DateUtil.offsetDay(today, -59), today, DateField.DAY_OF_MONTH);
                break;
            case "month3":
                dateRange = DateUtil.rangeToList(DateUtil.offsetDay(today, -89), today, DateField.DAY_OF_MONTH);
                break;
            default:
                dateRange = new ArrayList<>();
        }
        // x轴的日期数据
        //  datetimeToDateStr() 就是一个处理的方法， 把 DateTime类型的List转换成一个 String的List
        List<String> dateStrRange = datetimeToDateStr(dateRange);
        map.put("date", dateStrRange);

        // y轴的气象数据
        //  timeRange = week  month
        // getDataByTimeRange 不会统计数据库没有的日期，比如 数据库 11.4 这一天数据没有，他不会返回 date=2023-08-04的数据
        List<Weather> weatherDate = weatherMapper.getDataByTimeRange(timeRange);
        map.put("temperature", list(weatherDate, dateStrRange, 1)); // 1-temperature 2-humidity 3-rain_rate
        map.put("humidity", list(weatherDate, dateStrRange, 2));
        map.put("rain_rate", list(weatherDate, dateStrRange, 3));
        return map;
    }

    // 把 DateTime类型的List转换成一个 String的List
    private List<String> datetimeToDateStr(List<DateTime> dateTimeList) {
        List<String> list = CollUtil.newArrayList();
        if (CollUtil.isEmpty(dateTimeList)) {
            return list;
        }
        for (DateTime dateTime : dateTimeList) {
            String date = DateUtil.formatDate(dateTime);
            list.add(date);
        }
        return list;
    }

    // 对数据库未统计的时间进行处理
    private List<Double> list(List<Weather> weatherDate, List<String> dateRange, int num) {
        List<Double> dataList = CollUtil.newArrayList();
        if (CollUtil.isEmpty(weatherDate)) {
            return dataList;
        }
        for (String date : dateRange) {
            if (num == 1){
                // .map(Weather::getTemperature) 取出 对象里的 temperature值
                // orElse(0) 对没匹配的数据返回0 ("2023-08-24" 没有的话 就返回0)
                Double temperature = weatherDate.stream().filter(dataPO -> LocalDate.parse(date).equals(dataPO.getDate()))
                        .map(Weather::getTemperature).findFirst().orElse(0.0);
                dataList.add(temperature);
            }else if (num == 2){
                Double humidity = weatherDate.stream().filter(dataPO -> LocalDate.parse(date).equals(dataPO.getDate()))
                        .map(Weather::getHumidity).findFirst().orElse(0.0);
                dataList.add(humidity);
            }else if (num == 3){
                Double rainRate = weatherDate.stream().filter(dataPO -> LocalDate.parse(date).equals(dataPO.getDate()))
                        .map(Weather::getRainRate).findFirst().orElse(0.0);
                dataList.add(rainRate);
            }
        }
        return dataList;
    }
}
