package com.example.springboot.service;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Car;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ICarService {

    List<Car> list();

    PageInfo<Car> page(BaseRequest baseRequest);

    void save(Car car);

    Car getById(Integer id);

    void update(Car car);

    void deleteById(Integer id);

    void handlePesticide(Car car);

}
