package com.example.springboot.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Disease;
import com.example.springboot.exception.ServiceException;
import com.example.springboot.mapper.DiseaseMapper;
import com.example.springboot.service.IDiseaseService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
public class DiseaseService implements IDiseaseService {

    @Resource
    DiseaseMapper diseaseMapper;

    @Override
    public List<Disease> list() {
        return diseaseMapper.list();
    }

    @Override
    public PageInfo<Disease> page(BaseRequest baseRequest) {
        PageHelper.startPage(baseRequest.getPageNum(), baseRequest.getPageSize());
        return new PageInfo<>(diseaseMapper.listByCondition(baseRequest));
    }

    @Override
    public void save(Disease obj) {
        try {
            obj.setDiseaseCategory(diseaseCategoryToString(obj.getDiseaseCategories()));
            diseaseMapper.save(obj);
        } catch (Exception e) {
            throw new ServiceException("数据插入错误", e);
        }
    }

    @Override
    public Disease getById(Integer id) {
        return diseaseMapper.getById(id);
    }

    @Override
    public void update(Disease obj) {
        try {
            obj.setDiseaseCategory(diseaseCategoryToString(obj.getDiseaseCategories()));
            obj.setUpdatetime(LocalDate.now());
            diseaseMapper.updateById(obj);
        } catch (Exception e) {
            throw new ServiceException("数据更新错误", e);
        }
    }

    @Override
    public void deleteById(Integer id) {
        diseaseMapper.deleteById(id);
    }

    // 级联转储（转换字符串）
    private String diseaseCategoryToString(List<String> diseaseCategories) {
        StringBuilder sb = new StringBuilder();
        if (CollUtil.isNotEmpty(diseaseCategories)) {
            diseaseCategories.forEach(v -> sb.append(v).append(" > "));
            return sb.substring(0, sb.lastIndexOf(" > "));
        }
        return sb.toString();
    }

}
