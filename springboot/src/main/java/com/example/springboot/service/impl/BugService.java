package com.example.springboot.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Bug;
import com.example.springboot.exception.ServiceException;
import com.example.springboot.mapper.BugMapper;
import com.example.springboot.service.IBugService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
public class BugService implements IBugService {

    @Resource
    BugMapper bugMapper;

    @Override
    public List<Bug> list() {
        return bugMapper.list();
    }

    @Override
    public PageInfo<Bug> page(BaseRequest baseRequest) {
        PageHelper.startPage(baseRequest.getPageNum(), baseRequest.getPageSize());
        return new PageInfo<>(bugMapper.listByCondition(baseRequest));
    }

    @Override
    public void save(Bug obj) {
        try {
            obj.setBugCategory(bugCategoryToString(obj.getBugCategories()));
            bugMapper.save(obj);
        } catch (Exception e) {
            throw new ServiceException("数据插入错误", e);
        }
    }

    @Override
    public Bug getById(Integer id) {
        return bugMapper.getById(id);
    }

    @Override
    public void update(Bug obj) {
        try {
            obj.setBugCategory(bugCategoryToString(obj.getBugCategories()));
            obj.setUpdatetime(LocalDate.now());
            bugMapper.updateById(obj);
        } catch (Exception e) {
            throw new ServiceException("数据更新错误", e);
        }
    }

    @Override
    public void deleteById(Integer id) {
        bugMapper.deleteById(id);
    }

    // 级联转储（转换字符串）
    private String bugCategoryToString(List<String> bugCategories) {
        StringBuilder sb = new StringBuilder();
        if (CollUtil.isNotEmpty(bugCategories)) {
            bugCategories.forEach(v -> sb.append(v).append(" > "));
            return sb.substring(0, sb.lastIndexOf(" > "));
        }
        return sb.toString();
    }

}
