package com.example.springboot.service.impl;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Garden;
import com.example.springboot.entity.Completion;
import com.example.springboot.mapper.GardenMapper;
import com.example.springboot.service.IGardenService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
@Slf4j
public class GardenService implements IGardenService {

    @Resource
    GardenMapper gardenMapper;

    @Override
    public List<Garden> list() {
        return gardenMapper.list();
    }

    @Override
    public PageInfo<Garden> page(BaseRequest baseRequest) {
        PageHelper.startPage(baseRequest.getPageNum(), baseRequest.getPageSize());
        List<Garden> gardens = gardenMapper.listByCondition(baseRequest);
        for (Garden garden : gardens) {
            log.info(garden.toString());
            LocalDate startTime = garden.getStartTime();
            LocalDate endTime = garden.getEndTime();
            LocalDate now = LocalDate.now();
            if (now.plusDays(7).isAfter(endTime) && now.isBefore(endTime)) {   // 当前日期比结束日期小一周
                garden.setNote("即将成熟");
            } else if (now.isEqual(endTime) || now.isAfter(endTime)) {
                garden.setNote("已成熟");
            } else if (now.isBefore(startTime)){
                garden.setNote("未开始");
            } else {
                garden.setNote("生长中");
            }
        }
        return new PageInfo<>(gardens);
    }

    @Override
    @Transactional
    public void save(Garden obj) {
        obj.setEndTime(obj.getStartTime().plus(obj.getDays(), ChronoUnit.DAYS));  // 开始的日期加 days 得到归还的日期
        gardenMapper.save(obj);
    }

    @Override
    public PageInfo<Completion> pageCompletion(BaseRequest baseRequest) {
        PageHelper.startPage(baseRequest.getPageNum(), baseRequest.getPageSize());
        return new PageInfo<>(gardenMapper.listCompletionByCondition(baseRequest));
    }

//  确认收获逻辑
    @Transactional
    @Override
    public void saveCompletion(Completion obj) {
        // 改状态
        obj.setStatus("已收获");
        gardenMapper.updateStatus("已收获", obj.getId());  // obj.getId() 是前端传来的借书id
        obj.setRealTime(LocalDate.now());
        gardenMapper.saveCompletion(obj);
        gardenMapper.deleteById(obj.getId());
    }

    @Override
    public void backCompletion(Completion obj){
        obj.setStatus("种植中");
        gardenMapper.backCompletion(obj);
        gardenMapper.deleteCompletionById(obj.getId());
    }

    @Override
    public Garden getById(Integer id) {
        return gardenMapper.getById(id);
    }

    @Override
    public Garden getCompletionById(Integer id) {
        return gardenMapper.getCompletionById(id);
    }

    @Override
    public void update(Garden obj) {
        obj.setEndTime(obj.getStartTime().plus(obj.getDays(), ChronoUnit.DAYS));
        obj.setUpdateTime(LocalDate.now());
        gardenMapper.updateById(obj);
    }

    @Override
    public void deleteById(Integer id) {
        gardenMapper.deleteById(id);
    }

    @Override
    public void deleteCompletionById(Integer id) {
        gardenMapper.deleteCompletionById(id);
    }

}
