package com.example.springboot.service;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Disease;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IDiseaseService {
    
    List<Disease> list();

    PageInfo<Disease> page(BaseRequest baseRequest);

    void save(Disease obj);

    Disease getById(Integer id);

    void update(Disease obj);

    void deleteById(Integer id);

}
