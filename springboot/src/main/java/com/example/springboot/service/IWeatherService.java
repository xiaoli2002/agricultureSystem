package com.example.springboot.service;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Weather;

import java.util.List;
import java.util.Map;

public interface IWeatherService {

    List<Weather> list();

    List<Weather> listByCondition(BaseRequest baseRequest);

    Map<String, Object> getDataByTimeRange(String timeRange);
}
