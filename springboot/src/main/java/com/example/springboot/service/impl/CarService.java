package com.example.springboot.service.impl;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Car;
import com.example.springboot.mapper.CarMapper;
import com.example.springboot.service.ICarService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class CarService implements ICarService {

    @Autowired
    CarMapper carMapper;

    @Override
    public List<Car> list() {
        return carMapper.list();
    }

    @Override
    public PageInfo<Car> page(BaseRequest baseRequest) {
        PageHelper.startPage(baseRequest.getPageNum(), baseRequest.getPageSize());
        List<Car> cars = carMapper.listByCondition(baseRequest);
        return new PageInfo<>(cars);
    }

    @Override
    public void save(Car car) {
        car.setPesticide(0);
        carMapper.save(car);
    }

    @Override
    public Car getById(Integer id) {
        return carMapper.getById(id);
    }

    @Override
    public void update(Car car) {
        car.setUpdatetime(new Date());
        carMapper.updateById(car);
    }

    @Override
    public void deleteById(Integer id) {
        carMapper.deleteById(id);
    }

    @Override
    public void handlePesticide(Car car) {
        Integer recruitment = car.getRecruitment();
        if (recruitment == null) {
            return;
        }
        Integer id = car.getId();
        Car dbCar = carMapper.getById(id);
        dbCar.setPesticide(dbCar.getPesticide() + recruitment);
        carMapper.updateById(dbCar);
    }

}
