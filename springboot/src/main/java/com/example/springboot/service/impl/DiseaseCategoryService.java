package com.example.springboot.service.impl;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.DiseaseCategory;
import com.example.springboot.mapper.DiseaseCategoryMapper;
import com.example.springboot.service.IDiseaseCategoryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
public class DiseaseCategoryService implements IDiseaseCategoryService {

    @Resource
    DiseaseCategoryMapper diseaseCategoryMapper;

    @Override
    public List<DiseaseCategory> list() {
        return diseaseCategoryMapper.list();
    }

    @Override
    public PageInfo<DiseaseCategory> page(BaseRequest baseRequest) {
        PageHelper.startPage(baseRequest.getPageNum(), baseRequest.getPageSize());
        // 自关联查询
        List<DiseaseCategory> diseaseCategories = diseaseCategoryMapper.listByCondition(baseRequest);
        return new PageInfo<>(diseaseCategories);
    }

    @Override
    public void save(DiseaseCategory obj) {
        diseaseCategoryMapper.save(obj);
    }

    @Override
    public DiseaseCategory getById(Integer id) {
        return diseaseCategoryMapper.getById(id);
    }

    @Override
    public void update(DiseaseCategory obj) {
        obj.setUpdatetime(LocalDate.now());
        diseaseCategoryMapper.updateById(obj);
    }

    @Override
    public void deleteById(Integer id) {
        diseaseCategoryMapper.deleteById(id);
    }

}
