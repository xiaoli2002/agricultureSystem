package com.example.springboot.service.impl;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.BugCategory;
import com.example.springboot.mapper.BugCategoryMapper;
import com.example.springboot.service.IBugCategoryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
public class BugCategoryService implements IBugCategoryService {

    @Resource
    BugCategoryMapper bugCategoryMapper;

    @Override
    public List<BugCategory> list() {
        return bugCategoryMapper.list();
    }

    @Override
    public PageInfo<BugCategory> page(BaseRequest baseRequest) {
        PageHelper.startPage(baseRequest.getPageNum(), baseRequest.getPageSize());
        // 自关联查询
        List<BugCategory> bugCategories = bugCategoryMapper.listByCondition(baseRequest);
        return new PageInfo<>(bugCategories);
    }

    @Override
    public void save(BugCategory obj) {
        bugCategoryMapper.save(obj);
    }

    @Override
    public BugCategory getById(Integer id) {
        return bugCategoryMapper.getById(id);
    }

    @Override
    public void update(BugCategory obj) {
        obj.setUpdatetime(LocalDate.now());
        bugCategoryMapper.updateById(obj);
    }

    @Override
    public void deleteById(Integer id) {
        bugCategoryMapper.deleteById(id);
    }

}
