package com.example.springboot.service;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.Garden;
import com.example.springboot.entity.Completion;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IGardenService {
    
    List<Garden> list();

    PageInfo<Garden> page(BaseRequest baseRequest);

    void save(Garden obj);

    PageInfo<Completion> pageCompletion(BaseRequest baseRequest);

    void saveCompletion(Completion obj);

    void backCompletion(Completion obj);

    Garden getById(Integer id);

    Garden getCompletionById(Integer id);

    void update(Garden obj);

    void deleteById(Integer id);

    void deleteCompletionById(Integer id);

}
