package com.example.springboot.service;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.DiseaseCategory;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IDiseaseCategoryService {
    
    List<DiseaseCategory> list();

    PageInfo<DiseaseCategory> page(BaseRequest baseRequest);

    void save(DiseaseCategory obj);

    DiseaseCategory getById(Integer id);

    void update(DiseaseCategory obj);

    void deleteById(Integer id);

}
