package com.example.springboot.service;

import com.example.springboot.controller.request.BaseRequest;
import com.example.springboot.entity.BugCategory;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IBugCategoryService {
    
    List<BugCategory> list();

    PageInfo<BugCategory> page(BaseRequest baseRequest);

    void save(BugCategory obj);

    BugCategory getById(Integer id);

    void update(BugCategory obj);

    void deleteById(Integer id);

}
